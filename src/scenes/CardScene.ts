import { TweenLite, TweenMax } from 'gsap';
import { FpsCounter } from '../util/FpsCounter';
import { LinkedNode } from '../util/LinkedNode';
import { Timer } from '../util/Timer';
import { GameScene } from './GameScene';
import { UIContainer } from './UIContainer';
import { IGameScene } from '../interface/IGameScene';



export class CardScene extends GameScene implements IGameScene
{
    protected _fpsCounter: FpsCounter;
    protected _cards: LinkedNode[];
    protected _counter: number;
    protected _targetNode: LinkedNode;
    protected _targetX: number = 250;
    protected _timer: Timer;
    static CardLength: number = 144;

    constructor(uiContainer:UIContainer)
    {
        super(uiContainer);
        this._cards = [];
        this._counter = CardScene.CardLength - 1;
        this._gameObjectContainer.sortableChildren = true;
        this._fpsCounter = new FpsCounter(this);


        this.init();
        this._timer = new Timer(1000, () => this.run(), true);

    }

    /**
     * Link nodes, to determine where the cards need to move to.
     */
    protected init()
    {
        for (let i = 0; i < CardScene.CardLength; i++)
        {
            let node = new LinkedNode(this._gameSpriteSheet['diamond.png'], i * 10);
            let sprite = node.sprite;
            sprite.zIndex = i + 1;
            this._gameObjectContainer.addChild(sprite);
            this._cards.push(node);

            if (i > 0)
            {
                this._cards[i - 1].linkBefore(node);
            }
        }

        this._cards[CardScene.CardLength - 1].linkBefore(this._cards[0]);
        this._targetNode = this._cards[CardScene.CardLength - 1];
    }

    public start()
    {
            if (this._counter < 1)
            {
                this.reset();
            }
            this.run();
            this._timer.start();
    }

    /**
     * Resets everything and switches back to main menu
     */
    public return()
    {
        TweenMax.killAll();
        this._timer.kill();
        this.reset();
    }

    /**
     * Moves the cards, modify the zIndex to simulate proper stacking effect.
     */
    public move()
    {
        let count = this._counter;
        let targetY = this._targetNode.next.originY;
        let tweenable = this._cards[this._counter].sprite;

        TweenLite.to(tweenable, 2, { x: this._targetX, y: targetY});
        tweenable.zIndex = (CardScene.CardLength - 1) - count;
        this._targetNode = this._targetNode.next;
    }

    protected run()
    {
        if (this._counter >= 0)
        {
            this.move();
            this._counter--;
        }
        else
        {
            this._timer.kill();
        }
    }

    public reset()
    {
        this._targetNode = this._cards[CardScene.CardLength - 1];
        this._counter = CardScene.CardLength - 1;
        for (let i = 0; i < CardScene.CardLength; i++)
        {
            this._cards[i].sprite.zIndex = i;
            this._cards[i].sprite.x = 0;
            this._cards[i].sprite.y = this._cards[i].originY;
        }
    }
}